CREATE DATABASE GetFace;
USE GetFace;

CREATE TABLE Data (
_id VARCHAR(24) NOT NULL,
name VARCHAR(60),
bio VARCHAR(500),
gender CHAR(1),
distance INT,
birth_date DATE,

PRIMARY KEY (_id)
);

CREATE TABLE Photos (
_id VARCHAR(24) NOT NULL,
id_photo VARCHAR(36) NOT NULL,

PRIMARY KEY (id_photo),
CONSTRAINT FK_DataPhoto FOREIGN KEY (_id)
    REFERENCES Data(_id)
);

CREATE TABLE Instagram (
_id VARCHAR(24) NOT NULL,
username VARCHAR(64) NOT NULL,
last_conn VARCHAR(24),

PRIMARY KEY (username),
CONSTRAINT FK_DataInstagram FOREIGN KEY (_id)
    REFERENCES Data(_id)
);

CREATE TABLE School (
_id VARCHAR(24) NOT NULL,
id_school INT NOT NULL AUTO_INCREMENT,
name_sc VARCHAR(64) NOT NULL,

PRIMARY KEY (id_school),
CONSTRAINT FK_DataSchool FOREIGN KEY (_id)
    REFERENCES Data(_id)
);

CREATE TABLE Job (
_id VARCHAR(24) NOT NULL,
id_job INT NOT NULL AUTO_INCREMENT,
company VARCHAR(128),
title VARCHAR(128),

PRIMARY KEY (id_job),
CONSTRAINT FK_DataJob FOREIGN KEY (_id)
    REFERENCES Data(_id)
);
