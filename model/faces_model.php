<?php

/**
 *
 */
class Faces_Model
{

    private $conn;
    private $faces;

    function __construct()
    {
        require_once("model/connect.php");
        $this->conn = Connect::connection();
        $this->faces = array();
    }

    public function get_faces()
    {
        $query = $this->conn->query(
            "SELECT data._id, data.name, data.birth_date, data.distance, data.urlPhoto, photos.id_photo
                    FROM data
                    INNER JOIN photos ON data._id = photos._id
                    GROUP BY data._id
                    ORDER BY data.name
                    LIMIT 50;"
        );
        while ($rows = $query->fetch(PDO::FETCH_ASSOC)) {
            $this->faces[] = (object) $rows;    #array->_id;
            #$this->faces[] = $rows;            #array["_id"];
        }
        return $this->faces;
    }
}
