<?php

class Tinder
{

	protected $serviceUrl = 'https://api.gotinder.com/';
	protected $fbUserId;
	protected $fbToken;
	protected $authToken;
	protected $user;


	public function __construct($facebookUserId, $facebookToken)
	{
		$this->fbUserId = $facebookUserId;
		$this->fbToken = $facebookToken;

		$this->authenticate();
	}

	public function api($url, $data)
	{

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->serviceUrl . $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "Content-Type: application/json";
		$headers[] = "App_version: 3";
		$headers[] = "Platform: ios";
		$headers[] = "User-Agent: Tinder/3.0.4 (iPhone; iOS 7.1; Scale/2.00)";
		$headers[] = "Os_version: 700001";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		try {
			$result = curl_exec($ch);
		} catch (\Exception $e) {
			echo 'Error:' . curl_error($e);
			$result = "PASARON 30SEG";
		}

		curl_close($ch);

		return json_decode($result);
	}

	public function authenticate()
	{

		$response = $this->api('v2/auth/login/facebook', "{\"token\":\"$this->fbToken\"}");
		if ($response && isset($response->data->api_token)) {
			$this->authToken = $response->data->api_token;
			$this->user = $response->data->_id;
		}
	}

	public function api_post($url, $data)
	{

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->serviceUrl . $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = "X-Auth-Token: " . $this->authToken;
		$headers[] = "Content-Type: application/json";
		$headers[] = "User-Agent: Tinder/4.0.9 (iPhone; iOS 8.1.1; Scale/2.00)";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


		try {
			$result = curl_exec($ch);
		} catch (\Exception $e) {
			echo 'Error:' . curl_error($e);
			$result = "PASARON 30SEG";
		}

		curl_close($ch);

		return json_decode($result, true);
	}

	public function api_get($url)
	{

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->serviceUrl . $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		$headers = array();
		$headers[] = "X-Auth-Token: " . $this->authToken;
		$headers[] = "Content-Type: application/json";
		$headers[] = "User-Agent: Tinder/4.0.9 (iPhone; iOS 8.1.1; Scale/2.00)";
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		try {
			$result = curl_exec($ch);
		} catch (\Exception $e) {
			echo 'Error:' . curl_error($e);
			$result = "PASARON 30SEG";
		}

		curl_close($ch);

		var_dump(json_decode($result, true));

		return json_decode($result, true);
	}

	public function user($userId)
	{
		return $this->api('user/', $userId);
	}

	public function getUser()
	{
		return $this->user;
	}

	public function getAuthToken()
	{
		return $this->authToken;
	}

	public function recommendations()
	{
		return $this->api_get('user/recs');
	}

	public function updates()
	{
		return $this->api_post('updates', "{\"last_activity_date\": \"\" }");
	}

	public function updateLocation($lat, $lon)
	{
		return $this->api_post('user/ping', "{\"lat\": $lat, \"lon\": $lon}");
	}

	public function changePreferences($age_min, $gender, $my_gender, $age_max, $distance)
	{
		return $this->api_post('profile', "{\"age_filter_min\": $age_min, \"gender_filter\": $gender, \"gender\": $my_gender, \"age_filter_max\": $age_max, \"distance_filter\": $distance}");
	}

	public function getInfo($userId)
	{
		return $this->api_get('user/matches/' . $userId);
	}
}
