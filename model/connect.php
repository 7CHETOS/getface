<?php

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '7177');
define('DB_NAME', 'GetFace');
/**
 *
 */
class Connect
{
    public static function connection()
    {
        try {
            $connection = new PDO("mysql:host=" . DB_HOST . "; dbname=" . DB_NAME . ";charset=utf8mb4", DB_USER, DB_PASS);
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $connection->exec("set names utf8mb4");
        } catch (Exception $e) {

            die("Error: " . $e->getMessage());
        }

        return $connection;
    }
}
