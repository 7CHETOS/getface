<?php
/**
 *
 */

class Faces_Controller
{

    public function index()
    {
        require_once("model/faces_model.php");

        $faces = new Faces_Model();
        $array_faces = $faces->get_faces();

        require_once("view/_template/header.php");
        require_once("view/faces_view.php");
    }
}
 ?>
