<?php
/**
 *
 */

class AddFaces_Controller
{

    public function index()
    {
        require_once("model/add_faces_model.php");

        $add_faces = new AddFaces_Model();
        $log_add_faces = $add_faces->sort_information();

        require_once("view/_template/header.php");
        require_once("view/add_faces_view.php");
    }
}
 ?>
