<!DOCTYPE html>
<html>

<head>
    <title>GetFace Gallery Templates</title>
</head>

<body>

    <div class="pre-loading"></div>

    <div class="container gallery-container">

        <h1>Gallery GetFace</h1>

        <p class="page-description text-center">Thumbnails With Name And Description</p>

        <div class="tz-gallery">

            <div class="row">

                <?php if (isset($log_add_faces) and !empty($log_add_faces)) : ?>
                    <?php foreach ($log_add_faces as $key) : ?>
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <a class="lightbox" href="https://images-ssl.gotinder.com/<?= $key->id ?>/640x640_<?= $key->photo ?>.jpg">
                                    <img src="https://images-ssl.gotinder.com/<?= $key->id ?>/320x320_<?= $key->photo ?>.jpg" alt="<?= $key->id ?>">
                                </a>
                                <div class="caption">
                                    <?php if ($key->flag == 'Successfully') : ?>
                                        <h3><?= $key->name ?></h3>
                                        <p><b>Distance: </b><?= $key->distance ?> mi</p>
                                        <p><b>Birthdate: </b><?= $key->birthdate ?> </p>
                                        <p><b>Log: </b><?= $key->flag ?> </p>
                                    <?php else : ?>
                                        <h3><?= $key->name ?></h3>
                                        <p><b>Log: </b><?= $key->flag ?> </p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>

        </div>

    </div>

    <script>
        baguetteBox.run('.tz-gallery');
    </script>

</body>

</html>