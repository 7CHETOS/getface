<!DOCTYPE html>
<html>

<head>
    <title>GetFace Gallery Templates</title>
</head>

<body>

    <div class="pre-loading"></div>

    <div class="container gallery-container">

        <h1>Gallery GetFace</h1>

        <!-- <p class="page-description text-center">Thumbnails With Name And Description</p> -->

        <div class="tz-gallery">

            <div class="row">

                <?php if (isset($array_faces) and !empty($array_faces)) : ?>
                    <?php foreach ($array_faces as $key) : ?>
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <a class="lightbox" href="<?= $key->urlPhoto ?>">
                                    <img style="width: 270px; height:300px" src="<?= $key->urlPhoto ?>" alt="<?= $key->_id ?>">
                                </a>
                                <div class="caption">
                                    <h3><?= $key->name ?></h3>
                                    <p><b>Bith Date: </b><?= $key->birth_date ?></p>
                                    <p><b>Distance: </b><?= $key->distance ?> mi</p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

            </div>

        </div>

    </div>

    <script>
        baguetteBox.run('.tz-gallery');
    </script>

</body>

</html>